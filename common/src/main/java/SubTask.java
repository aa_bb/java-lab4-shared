import java.io.Serializable;

public class SubTask implements Serializable {
    protected String name;
    protected String description;
    protected boolean isFinished;

    public void setName(String name) {
        this.name = name;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    public SubTask(String name, String description, boolean isFinished) {
        this.name = name;
        this.description = description;
        this.isFinished = isFinished;
    }

    public SubTask()
    {
        name = "Новая подзадача";
        description = "";
        isFinished = false;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public boolean isFinished() {
        return isFinished;
    }
}
