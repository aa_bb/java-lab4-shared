import javafx.beans.InvalidationListener;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.regex.Pattern;

public class Task extends SubTask implements Serializable {
    transient Optional<LocalDate> deadline;
    ArrayList<String> tags;
    ArrayList<SubTask> subTasks;

    //Optional<LocalDate> ld;
    public void setSubTasks(ArrayList<SubTask> subTasks) {
        this.subTasks = subTasks;
    }

    public ArrayList<SubTask> getSubTasks() {
        return subTasks;
    }

//    public void addSubTask(SubTask subTask)
//    {
//        if(subTasks == null)
//            subTasks = new ArrayList<SubTask>();
//        subTasks.add(subTask);
//    }

    public Task(String name, String description, boolean isFinished,
                Optional<LocalDate> deadline, ArrayList<String> tags, ArrayList<SubTask> subTasks) {
        super(name, description, isFinished);
        this.deadline = deadline;
        this.tags = tags;
        this.subTasks = subTasks;
    }

    public void setDeadline(Optional<LocalDate> deadline) {
        this.deadline = deadline;
    }

    public void setTags(ArrayList<String> tags) {
        this.tags = tags;
    }

    public boolean containsTag(String tag) {
        //ФП
        return tags.stream().anyMatch(t -> t.equalsIgnoreCase(tag));
    }

    public boolean containsSubStrInDescription(String subStr) {
        return Pattern
                .compile(subStr, Pattern.CASE_INSENSITIVE + Pattern.UNICODE_CASE)
                .matcher(description).find();
    }


    public Task() {
        name = "Новая задача";
        description = "";
        isFinished = false;
        //НеФП
        deadline = Optional.empty();
        tags = new ArrayList<String>();
        subTasks = new ArrayList<SubTask>();
    }

    public LocalDate getDeadline() {
        return deadline.orElse(null);
    }

    public Optional<LocalDate> getOptionalDeadline()
    {
        return deadline;
    }

    public List<String> getTags() {
        return tags;
    }

    public StringProperty getStringTags() {
        StringBuilder sbTags = new StringBuilder();
        //ФП
        tags.forEach(t -> sbTags.append(t).append(','));

        //НеФП убираем лишнюю запятую
        if (sbTags.length() > 0)
            sbTags.deleteCharAt(sbTags.length() - 1);

        return new SimpleStringProperty(sbTags.toString());//new StringProperty(sbTags.toString()) ;
    }
    private void writeObject(ObjectOutputStream oos) throws IOException {
        oos.defaultWriteObject();
        LocalDate ld;
        if(deadline.isPresent())
            ld = deadline.get();
        else
            ld = null;
        //LocalDate ld = deadline.orElse(null);
        oos.writeObject(ld);//deadline.orElse(null)
    }

    private void readObject(ObjectInputStream in) throws IOException, ClassNotFoundException {
        in.defaultReadObject();
        LocalDate ld = (LocalDate) in.readObject();
        deadline = Optional.ofNullable(ld);
    }
}
