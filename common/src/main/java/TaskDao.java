import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Optional;
/*Существует всего 2 способа писать код без багов. Но работает почему-то третий*/

public interface TaskDao {
    ArrayList<Task> getAll();
    void addTask(Task newTask);
    void changeTask(Task changingTask,
                    String name,
                    String description,
                    boolean isFinished,
                    Optional<LocalDate> deadline,
                    ArrayList<String> tags,
                    ArrayList<SubTask> subTasks
                    );
    void deleteTask(Task task);
    void saveStateTasks();

    //Фильтры
    //незавершенные задачи
    ArrayList<Task> getUnfinishedTasks();
    //незавершённые просроченные задач и задачи со сроком выполнения в ближайшую неделю
    ArrayList<Task> getUnfinishedOverdueAndDeadlineInNearWeekTasks();
    //незавершённые задачи, отмеченные выбранным тегом
    ArrayList<Task> getUnfinishedTasksByTag(String tag);
    //завершенные задачи
    ArrayList<Task> getFinishedTasks();

    //задачи на ближайший месяц, помеченные выбираемым тегом и содержащие в тексте описания указываемую подстроку
    ArrayList<Task> getOnNearestMonthWithTagAndSubStr(String tag, String subStr);
    //незавершённые задачи, в которых по крайней мере половина подзадач завершена
    ArrayList<Task> getUnfinishedMoreHalfSubTasksFinished();
    //просроченные задачи, отмеченные по крайней мере одним из трёх самых часто используемых тегов среди всех задач
    ArrayList<Task> getOverdueWithOneMoreTagFromThreePopular();
    //три задачи с ближайшим крайним сроком, отмеченные выбираемым тегом
    ArrayList<Task> getThreeWithNearestDeadlineWithTag(String tag);
    //четыре задачи с самым отдалённым крайним сроком, не отмеченные никаким тегом
    ArrayList<Task> getFourWithFarDeadlineWithoutTag();
}
