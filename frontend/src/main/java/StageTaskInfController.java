import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TextField;
import javafx.scene.control.TextArea;
import javafx.scene.control.CheckBox;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.util.StringConverter;
import jdk.nashorn.internal.runtime.Property;

import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Optional;

public class StageTaskInfController {


    TableView<Task> tvTasksFromMainStage;
    Task task;
    Stage stageTaskIfn;

    public void setMainStageController(MainStageController mainStageController) {
        this.mainStageController = mainStageController;
    }

    MainStageController mainStageController;
    stageInfType stageType;

    @FXML
    private Button btnOk;

    @FXML
    private CheckBox cbIsDeadline;

    @FXML
    private DatePicker dpDeadline;

    @FXML
    private TextField tfNameTask;

    @FXML
    private ListView<String> lvTags;

    @FXML
    private TextField tfNewTag;

    @FXML
    private TextArea taDescription;

    @FXML
    private CheckBox cbIsFinished;

    @FXML
    private TableView<SubTask> tvSubTasks;

    @FXML
    private TableColumn<SubTask, String> tcNameSubTask;

    @FXML
    private TableColumn<SubTask, String> tcDescriptionSubTask;

    @FXML
    private TableColumn<SubTask, Boolean> tcIsFinishedSubTask;

    @FXML
    private Button btnAddTag;

    @FXML
    private Button btnChangeTag;

    @FXML
    private Button btnDeleteTag;

    @FXML
    private Button btnAddSubTask;

    @FXML
    private Button btnChangeSubTask;

    @FXML
    private Button btnDeleteSubTask;

    @FXML
    private Button btnSwapSubTasks;

    public void setAllControlsDisabled() {
        btnOk.setVisible(false);
        cbIsDeadline.setDisable(true);
        dpDeadline.setDisable(true);
        dpDeadline.setStyle("-fx-opacity: 1");
        dpDeadline.getEditor().setStyle("-fx-opacity: 1");

        tfNameTask.setEditable(false);
        tfNewTag.setEditable(false);
        taDescription.setEditable(false);
        cbIsFinished.setDisable(true);

        btnAddTag.setVisible(false);
        btnChangeTag.setVisible(false);
        btnDeleteTag.setVisible(false);

        btnChangeSubTask.setVisible(false);
        btnAddSubTask.setVisible(false);
        btnDeleteSubTask.setVisible(false);
        btnSwapSubTasks.setVisible(false);
    }

    public void setTask(Task task) {
        this.task = task;
        //НеФп
        if (stageType != stageInfType.add) {
            tfNameTask.setText(task.getName());
            taDescription.setText(task.getDescription());
            cbIsFinished.setSelected(task.isFinished());
            //ФП
            boolean isDeadline = task.getOptionalDeadline().isPresent();
            cbIsDeadline.setSelected(isDeadline);
            dpDeadline.setVisible(isDeadline);
            dpDeadline.setValue(task.getOptionalDeadline().orElse(LocalDate.now()));

            lvTags.getItems().addAll(task.getTags());

            tvSubTasks.getItems().addAll(task.getSubTasks());
        }
    }

    public void setStageType(stageInfType stageType) {
        this.stageType = stageType;
        //НеФп
        if (stageType == stageInfType.add) {
            stageTaskIfn.setTitle("Добавление задачи");
            btnOk.setText("Добавить");
            dpDeadline.setValue(LocalDate.now());
            return;
        }
        //НеФп
        if (stageType == stageInfType.change) {
            stageTaskIfn.setTitle("Редактирование задачи");
            btnOk.setText("Сохранить");
            return;
        }

        //if(stageType == stageInfType.view)
        setAllControlsDisabled();
        stageTaskIfn.setTitle("Просмотр задачи");

    }

    public void saveAndExit() {
        String name = tfNameTask.getText().trim();
        //НеФП
        if (name.equals("")) {
            showAlertError("Ошибка в названии задачи",
                    "Название не может состоять из пробельных символов или быть пустым");
            tfNameTask.setText("");
            return;
        } else
            task.setName(name);

        task.setDescription(taDescription.getText().trim());
        //НеФП
        if (cbIsDeadline.isSelected())
            task.setDeadline(Optional.of(dpDeadline.getValue()));
        else
            task.setDeadline(Optional.empty());

        task.setFinished(cbIsFinished.isSelected());

        task.setTags(new ArrayList<String>(lvTags.getItems()));

        //НеФП
        if (stageType == stageInfType.add)
            mainStageController.addTask(task);
        else
            mainStageController.refreshLvTasks();

        task.setSubTasks(new ArrayList<SubTask>(tvSubTasks.getItems()));

        stageTaskIfn.close();
    }

    public void addSubTask(SubTask subTask) {
        tvSubTasks.getItems().add(subTask);
    }

    @FXML
    void initialize() {
        lvTags.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        tvSubTasks.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        tcNameSubTask.setCellValueFactory(new PropertyValueFactory<SubTask, String>("name"));

        tcDescriptionSubTask.setCellValueFactory(new PropertyValueFactory<SubTask, String>("description"));

        tcIsFinishedSubTask.setCellValueFactory(new PropertyValueFactory<SubTask, Boolean>("isFinished"));

        //делаем столбец со свойсвом завершена типа checkBox
        MainStageController.setCustomTcIsFinished(tcIsFinishedSubTask);

        setCustomDateFormatInDpDeadline();
    }

    private void setCustomDateFormatInDpDeadline() {
        StringConverter<LocalDate> converter = new StringConverter<LocalDate>() {
            DateTimeFormatter dateFormatter =
                    DateTimeFormatter.ofPattern("yyyy-MM-dd");

            @Override
            public String toString(LocalDate date) {
                return dateFormatter.format(date);
            }

            @Override
            public LocalDate fromString(String string) {
                return LocalDate.parse(string, dateFormatter);
            }
        };
        dpDeadline.setConverter(converter);
        dpDeadline.setPromptText("yyyy-MM-dd");
    }

    public void setStageTaskIfn(Stage stageTaskIfn) {
        this.stageTaskIfn = stageTaskIfn;
    }

    public static void showAlertError(String headerText, String contentText) {
        Alert alert = new Alert(Alert.AlertType.ERROR);
        alert.setTitle("Ошибка");
        alert.setHeaderText(headerText);
        alert.setContentText(contentText);
        alert.showAndWait();
    }

    public void OnActionDpDeadline(ActionEvent actionEvent) {

        if (dpDeadline.getValue().isBefore(LocalDate.now())) {
            {
                //showAlertError("Ошибка установки крайнего срока для задачи",
                        //"Нельзя ставить задачу на прошлое!");
                //dpDeadline.setValue(LocalDate.now());
            }
        }
    }

    public void OnActionBtnOk(ActionEvent actionEvent) {
        saveAndExit();
    }

    public boolean isNewTagCorrect() {
        String newTag = tfNewTag.getText().trim();
        //НеФП
        if (newTag.equals("")) {
            showAlertError("Ошибка в названии тега", "Тег не может состоять из пробельных символов или быть пустым");
            tfNewTag.setText("");
            return false;
        }


        //НеФП
        //ФП
        if (lvTags.getItems().stream().anyMatch(tag -> tag.equalsIgnoreCase(newTag))) {
            showAlertError("Ошибка в названии тега", "Тег с названием " + "\"" + newTag + "\"" + "уже существует");
            tfNewTag.setText(newTag);
            return false;
        }
        return true;
    }

    public void OnActionBtnAddTag(ActionEvent actionEvent) {
        //НеФП
        if (isNewTagCorrect())
            lvTags.getItems().add(tfNewTag.getText().trim());
    }

    public void OnActionBtnDeleteTag(ActionEvent actionEvent) {
        //НеФП
        if (lvTags.getSelectionModel().getSelectedItems().size() == 0) {
            showAlertError("Ошибка количества выбранных тегов для удаления", "Выберите не менее одного тега");
            return;
        }
        lvTags.getItems().removeAll(lvTags.getSelectionModel().getSelectedItems());
    }

    public void showStageSubTaskInf(stageInfType stageType) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/stageSubTaskInformation.fxml"));
        Parent root = new Parent() {
        };
        try {
            root = (Parent) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage stageSubTaskInf = new Stage();

        stageSubTaskInf.setScene(new Scene(root, 273, 315));

        stageSubTaskInf.initModality(Modality.WINDOW_MODAL);

        stageSubTaskInf.initOwner(stageTaskIfn);

        StageSubTaskInfController stageSubTaskInfController = loader.getController();
        //передаем в контроллер объект окна с которым он работает
        stageSubTaskInfController.setStageSubTaskInf(stageSubTaskInf);
        //передаем контроллер окна для задачи в контроллер для подзадачи
        stageSubTaskInfController.setStageTaskInfController(this);

        //открываем окно в нужном режиме
        stageSubTaskInfController.setStageType(stageType);

        //если добавляем задачу то отправляем новую
        //НеФП
        if (stageType == stageInfType.add)
            stageSubTaskInfController.setSubTask(new SubTask());
        else
            //при редактировании отправляем редактируемую подзадачу
            //НеФП
            if (stageType == stageInfType.change || stageType == stageInfType.view)
                stageSubTaskInfController.setSubTask(tvSubTasks.getSelectionModel().getSelectedItem());

        stageSubTaskInf.show();
    }


    public void OnActionBtnAddSubTask(ActionEvent actionEvent) {

//        if(cbIsFinished.isSelected())
//        {
//            cbIsFinished.setSelected(false);
//            Alert alert = new Alert(Alert.AlertType.INFORMATION);
//            alert.setTitle("Уведомление");
//            alert.setHeaderText("Задача становится незавершенной");
//            alert.setContentText("Т.к вы добавляете новую подзадачу к завершенной задаче, то задача перестает быть завершенной");
//            alert.showAndWait();
//        }

        showStageSubTaskInf(stageInfType.add);
    }

    public void refreshTvSubTasks() {
        tvSubTasks.refresh();
    }

    public boolean isNameSubTaskUnique(SubTask newSubTask) {
        //ФП
       return !tvSubTasks.getItems().stream()
                .anyMatch(subTask ->
                                subTask.getName().equalsIgnoreCase(newSubTask.getName())
                                && subTask != newSubTask);
    }

    public void OnActionBtnChangeSubTask(ActionEvent actionEvent) {
        //НеФП
        if (tvSubTasks.getSelectionModel().getSelectedItems().size() == 0) {
            StageTaskInfController.showAlertError("Ошибка количества выбранных подзадач для редактирования",
                    "Выберите одну подзадачу");
            return;
        }
        showStageSubTaskInf(stageInfType.change);
    }

    public void OnActionCbIsFinished(ActionEvent actionEvent) {
        //НеФП
        if (cbIsFinished.isSelected()) {
            //ФП
            tvSubTasks.getItems()
                    .forEach(st -> st.setFinished(cbIsFinished.isSelected()));
            tvSubTasks.refresh();
        }
    }

    public void OnActionBtnDeleteSubTask(ActionEvent actionEvent) {
        //НеФП
        if (tvSubTasks.getSelectionModel().getSelectedItems().size() == 0) {
            StageTaskInfController.showAlertError("Ошибка количества выбранных подзадач для удаления",
                    "Выберите не менее одной подзадачи");
            return;
        }
        tvSubTasks.getItems().removeAll(tvSubTasks.getSelectionModel().getSelectedItems());
        tvSubTasks.getSelectionModel().clearSelection();
    }

    public void OnActionBtnSwapSubTasks(ActionEvent actionEvent) {
        //НеФП
        if (tvSubTasks.getSelectionModel().getSelectedItems().size() != 2) {
            StageTaskInfController.showAlertError("Ошибка количества выбранных подзадач для смены местами",
                    "Выберите 2 подзадачи");
            return;
        }
        SubTask t1 = tvSubTasks.getSelectionModel().getSelectedItems().get(0);
        SubTask t2 = tvSubTasks.getSelectionModel().getSelectedItems().get(1);

        int index1 = tvSubTasks.getItems().indexOf(t1);
        int index2 = tvSubTasks.getItems().indexOf(t2);

        SubTask temp;
        temp = t1;

        tvSubTasks.getItems().set(index1, t2);
        tvSubTasks.getItems().set(index2, temp);
    }

    public void OnActionBtnChangeTag(ActionEvent actionEvent) {
        //НеФП
        if (lvTags.getSelectionModel().getSelectedItems().size() != 1) {
            StageTaskInfController.showAlertError("Ошибка количества выбранных тегов для изменения",
                    "Выберите один тег");
            return;
        }

        String newTag = tfNewTag.getText().trim();

        //НеФП
        if (lvTags.getSelectionModel().getSelectedItem().equalsIgnoreCase(newTag) || isNewTagCorrect())
            lvTags.getItems().set((lvTags.getSelectionModel().getSelectedIndex()), newTag);
    }

    public void OnActionBtnViewSubTask(ActionEvent actionEvent) {
        //НеФП
        if (tvSubTasks.getSelectionModel().getSelectedItems().size() != 1) {
            StageTaskInfController.showAlertError("Ошибка количества выбранных подзадач для просмотра",
                    "Выберите одну подзадачу");
            return;
        }
        showStageSubTaskInf(stageInfType.view);
        //tvSubTasks.getSelectionModel().clearSelection();
    }

    public void OnActionCbIsDeadline(ActionEvent actionEvent) {
        dpDeadline.setVisible(cbIsDeadline.isSelected());
        dpDeadline.setValue(task.getOptionalDeadline().orElse(LocalDate.now()));
    }
}
