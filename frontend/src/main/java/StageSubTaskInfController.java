import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import javafx.stage.Stage;

public class StageSubTaskInfController {
    Stage stageSubTaskInf;
    SubTask subTask;
    stageInfType stageType;
    StageTaskInfController stageTaskInfController;

    public Stage getStageSubTaskInf() {
        return stageSubTaskInf;
    }

    public void setStageSubTaskInf(Stage stageSubTaskInf) {
        this.stageSubTaskInf = stageSubTaskInf;
    }

    public SubTask getSubTask() {
        return subTask;
    }

    public void setSubTask(SubTask subTask) {
        this.subTask = subTask;
        //НеФП
        if (stageType == stageInfType.change || stageType == stageInfType.view) {
            tfName.setText(subTask.getName());
            taDescription.setText(subTask.getDescription());
            cbIsFinished.setSelected(subTask.isFinished());
        }
    }

    @FXML
    private TextField tfName;

    @FXML
    private Label lblName;

    @FXML
    private Label lblDescription;

    @FXML
    private Label lbFinished;

    @FXML
    private TextArea taDescription;

    @FXML
    private CheckBox cbIsFinished;

    @FXML
    private Button btnOk;

    public void setStageTaskInfController(StageTaskInfController stageTaskInfController) {
        this.stageTaskInfController = stageTaskInfController;
    }

    public void setStageType(stageInfType stageType) {
        this.stageType = stageType;
        //НеФП
        if (stageType == stageInfType.add) {
            stageSubTaskInf.setTitle("Добавление подзадачи");
            btnOk.setText("Добавить");
            return;
        }
        //НеФП
        if (stageType == stageInfType.change) {
            stageSubTaskInf.setTitle("Редактирование подзадачи");
            btnOk.setText("Сохранить");
            return;
        }
        //if (stageType == stageInfType.view)
        stageSubTaskInf.setTitle("Просмотр подзадачи");
        setAllControlsDisabled();
    }

    public void setAllControlsDisabled() {
        tfName.setEditable(false);
        taDescription.setEditable(false);
        btnOk.setVisible(false);
        cbIsFinished.setDisable(true);
    }

    @FXML
    void initialize() {

    }

    public void OnActionBtnOk(ActionEvent actionEvent) {
        String name = tfName.getText().trim();
        //НеФП
        if (name.equals("")) {
            stageTaskInfController.showAlertError("Ошибка в названии подзадачи",
                    "Название не может состоять из пробельных символов или быть пустым");
            tfName.setText("");
            return;
        }
        subTask.setName(name);
        //НеФП
        if (!stageTaskInfController.isNameSubTaskUnique(subTask)) {
            stageTaskInfController.showAlertError("Ошибка в названии подзадачи",
                    "Уже существует подзадача с названием \"" + name + "\" , выберите другое название");
            tfName.setText(name);
            return;
        }

        subTask.setDescription(taDescription.getText().trim());


        subTask.setFinished(cbIsFinished.isSelected());
        //НеФП
        if (stageType == stageInfType.add)
            stageTaskInfController.addSubTask(subTask);
        else
            stageTaskInfController.refreshTvSubTasks();

        stageSubTaskInf.close();
    }
}
