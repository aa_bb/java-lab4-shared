import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

import java.io.IOException;

public class TaskManagerUI extends Application{
    static TaskDao taskDao;
    @Override
    public void start(Stage primaryStage) throws IOException {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/mainStage.fxml"));
        Parent root = (Parent)loader.load();

        MainStageController controller = loader.getController();
        controller.setMainStage(primaryStage);
        controller.setTaskDao(taskDao);

        primaryStage.setTitle("Менеджер задач");
        primaryStage.setScene(new Scene(root, 600, 400));
        primaryStage.show();
    }

    public static void showUI(String[] args, TaskDao someTaskDao)
    {
        taskDao = someTaskDao;
        launch(args);
    }
}
