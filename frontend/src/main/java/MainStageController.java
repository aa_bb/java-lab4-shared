import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.util.Callback;

import java.io.*;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MainStageController {
    TaskDao taskDao;
    Stage mainStage;

    //private ArrayList<Task> allTasks;
    private boolean isFiltredTasksInTvTasks;

    public void setTaskDao(TaskDao taskDao)
    {
        this.taskDao = taskDao;


        tvTasks.getItems().addAll(taskDao.getAll());
        isFiltredTasksInTvTasks = false;

        tvTasks.getSortOrder().addAll(tcDeadline, tcNameTask);
    }

    public void setMainStage(Stage mainStage) {
        this.mainStage = mainStage;
        mainStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
            public void handle(WindowEvent we) {
                taskDao.saveStateTasks();
            }
        });
    }


    @FXML
    private TableView<Task> tvTasks;

    @FXML
    private TableColumn<Task, String> tcNameTask;

    @FXML
    private TableColumn<Task, String> tcDescription;

    @FXML
    private TableColumn<Task, LocalDate> tcDeadline;

    @FXML
    private TableColumn<Task, String> tcTags;

    @FXML
    private TableColumn<Task, Boolean> tcIsFinished;

//    @FXML
//    private ComboBox<Filter> cbFilters;

    @FXML
    void initialize() {

        tcNameTask.setCellValueFactory(new PropertyValueFactory<Task, String>("name"));
        tcNameTask.setSortType(TableColumn.SortType.ASCENDING);

        tcDescription.setCellValueFactory(new PropertyValueFactory<Task, String>("description"));

        tcDeadline.setCellValueFactory(new PropertyValueFactory<Task, LocalDate>("deadline"));
        tcDeadline.setSortType(TableColumn.SortType.DESCENDING);

        tcTags.setCellValueFactory(cellData -> cellData.getValue().getStringTags());

        setCustomTcIsFinished(tcIsFinished);

        tvTasks.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
    }

    Predicate<TableView<Task>> zeroSelectedTasks = tv -> tvTasks.getSelectionModel().getSelectedItems().size() == 0;

    //сделать столбец с CheckBox для свойства задачи завершена
    public static void setCustomTcIsFinished(TableColumn tableColumn) {
        tableColumn.setCellValueFactory(new Callback<TableColumn.CellDataFeatures<Task, Boolean>, ObservableValue<Boolean>>() {

            @Override
            public ObservableValue<Boolean> call(TableColumn.CellDataFeatures<Task, Boolean> param) {
                SubTask task = param.getValue();

                SimpleBooleanProperty booleanProp = new SimpleBooleanProperty(task.isFinished());

                booleanProp.addListener(new ChangeListener<Boolean>() {

                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue,
                                        Boolean newValue) {
                        task.setFinished(newValue);
                    }
                });
                return booleanProp;
            }
        });

        tableColumn.setCellFactory(new Callback<TableColumn<Task, Boolean>,
                TableCell<Task, Boolean>>() {
            @Override
            public TableCell<Task, Boolean> call(TableColumn<Task, Boolean> p) {
                CheckBoxTableCell<Task, Boolean> cell = new CheckBoxTableCell<Task, Boolean>();
                cell.setAlignment(Pos.CENTER);
                return cell;
            }
        });
    }

    //при изменении задачи
    public void refreshLvTasks() {
        //НеФП
        if (isFiltredTasksInTvTasks) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Уведомление");
            alert.setHeaderText("Откат ко всем задачам");
            alert.setContentText("Т.к вы редактируете задачу при поиске, то откатываемся ко всем задачам");
            alert.showAndWait();
            tvTasks.getItems().clear();
            tvTasks.getItems().addAll(taskDao.getAll());
            isFiltredTasksInTvTasks = false;
        } else
            tvTasks.refresh();
    }

    public void addTask(Task newTask) {
        taskDao.addTask(newTask);
        //allTasks.add(newTask);
        if (isFiltredTasksInTvTasks) {
            Alert alert = new Alert(Alert.AlertType.INFORMATION);
            alert.setTitle("Уведомление");
            alert.setHeaderText("Откат ко всем задачам");
            alert.setContentText("Т.к вы добавляете новую задачу при поиске, то откатываемся ко всем задачам");
            alert.showAndWait();
            tvTasks.getItems().clear();
            tvTasks.getItems().addAll(taskDao.getAll());
            isFiltredTasksInTvTasks = false;
        } else
            tvTasks.getItems().add(newTask);
    }

    public void OnActionMiDeleteTask(ActionEvent actionEvent) {
        //НеФП if но с предикатом
        if(
                zeroSelectedTasks.test(tvTasks)
                //tvTasks.getSelectionModel().getSelectedItems().size() == 0
        )
        {
            StageTaskInfController.showAlertError("Ошибка количества выбранных задач для удаления",
                    "Выберите не менее одной задачи");
            return;
        }
        for (Task task: tvTasks.getSelectionModel().getSelectedItems()) {
            taskDao.deleteTask(task);
        }
        //allTasks.removeAll(tvTasks.getSelectionModel().getSelectedItems());
        tvTasks.getItems().removeAll(tvTasks.getSelectionModel().getSelectedItems());
        tvTasks.getSelectionModel().clearSelection();
    }

    public void OnActionMiAddTask(ActionEvent actionEvent) {
        showStageTaskInf(stageInfType.add);
    }

    public void showStageTaskInf(stageInfType stageType) {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("/stageTaskInformation.fxml"));

        Parent root = new Parent() {
        };
        try {
            root = (Parent) loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }

        Stage stageTaskInf = new Stage();

        stageTaskInf.setScene(new Scene(root, 764, 399));

        stageTaskInf.initModality(Modality.WINDOW_MODAL);

        stageTaskInf.initOwner(mainStage);

        StageTaskInfController stageTaskInfController = loader.getController();
        //передаем в контроллер объект окна с которым он работает
        stageTaskInfController.setStageTaskIfn(stageTaskInf);
        //передаем контроллер главного окна в контроллер дочернего
        stageTaskInfController.setMainStageController(this);

        //открываем окно в нужном режиме
        stageTaskInfController.setStageType(stageType);

        //НеФП
        //если добавляем задачу то отправляем новую
        if (stageType == stageInfType.add)
            stageTaskInfController.setTask(new Task());
        else if (stageType == stageInfType.change || stageType == stageInfType.view)
            //при редактировании отправляем редактируемую задачу
            stageTaskInfController.setTask(tvTasks.getSelectionModel().getSelectedItem());
//            for(Task task: tvTasks.getSelectionModel().getSelectedItems()) {
//                stageTaskInfController.setTask(task);
//
//            }
        stageTaskInf.show();
    }

    Predicate<TableView<Task>> notOneSelectedTask = tv -> tvTasks.getSelectionModel().getSelectedItems().size() != 1;
    public void OnActionMiChangeTask(ActionEvent actionEvent) {
        //НеФП
        //if но с предикатом
        if (
                notOneSelectedTask.test(tvTasks)
                //tvTasks.getSelectionModel().getSelectedItems().size() != 1
        ) {
            StageTaskInfController.showAlertError("Ошибка количества выбранных задач для редактирования",
                    "Выберите одну задачу");
            tvTasks.getSelectionModel().clearSelection();
            return;
        }
        showStageTaskInf(stageInfType.change);
    }

    public void OnActionMiViewTask(ActionEvent actionEvent) {
        //НеФП
        //if но с предикатом
        if (
                notOneSelectedTask.test(tvTasks)
                //tvTasks.getSelectionModel().getSelectedItems().size() != 1
        ) {
            StageTaskInfController.showAlertError("Ошибка количества выбранных задач для просмотра",
                    "Выберите одну задачу");
            tvTasks.getSelectionModel().clearSelection();
            return;
        }
        showStageTaskInf(stageInfType.view);
    }

    public void OnActionMiUnfinishedSortNameAsc(ActionEvent actionEvent) {
        tvTasks.getItems().clear();
        tvTasks.getItems().addAll(taskDao.getUnfinishedTasks());
        tvTasks.getSortOrder().clear();
        tcNameTask.setSortType(TableColumn.SortType.ASCENDING);
        tvTasks.getSortOrder().addAll(tcNameTask);
        isFiltredTasksInTvTasks = true;
    }

    public void OnActionMiFinishedSortNameAsc(ActionEvent actionEvent) {
        tvTasks.getItems().clear();
        tvTasks.getItems().addAll(taskDao.getFinishedTasks());
        tvTasks.getSortOrder().clear();
        tcNameTask.setSortType(TableColumn.SortType.ASCENDING);
        tvTasks.getSortOrder().addAll(tcNameTask);
        isFiltredTasksInTvTasks = true;
    }

    public void OnActionMiAlltasks(ActionEvent actionEvent) {
        tvTasks.getItems().clear();
        tvTasks.getItems().addAll(taskDao.getAll());
        isFiltredTasksInTvTasks = false;
    }

    public void OnActionMiUnfinishedOverdueAndDeadlineInNearWeekDeadlineAskNameAsk(ActionEvent actionEvent) {

        tvTasks.getItems().clear();
        tvTasks.getItems().addAll(taskDao.getUnfinishedOverdueAndDeadlineInNearWeekTasks());
        tvTasks.getSortOrder().clear();
        tcNameTask.setSortType(TableColumn.SortType.ASCENDING);
        tcDeadline.setSortType(TableColumn.SortType.ASCENDING);
        tvTasks.getSortOrder().addAll(tcDeadline, tcNameTask);
        isFiltredTasksInTvTasks = true;
    }

    public void OnActionMiUnfinishedMoreHalfSubTasksFinished(ActionEvent actionEvent) {
        tvTasks.getItems().clear();
        tvTasks.getItems().addAll(taskDao.getUnfinishedMoreHalfSubTasksFinished());
        tvTasks.getSortOrder().clear();
        tcNameTask.setSortType(TableColumn.SortType.ASCENDING);
        tcDeadline.setSortType(TableColumn.SortType.ASCENDING);
        tvTasks.getSortOrder().addAll(tcDeadline, tcNameTask);
        isFiltredTasksInTvTasks = true;
    }


    public void OnActionMiUnfinishedByTagNameAsk(ActionEvent actionEvent) {

        String tagName;
        TextInputDialog dialog = new TextInputDialog();

        dialog.setTitle("Поиск по тэгу");
        dialog.setHeaderText("");
        dialog.setContentText("Тэг:");
        //ФП
        Optional<String> result = dialog.showAndWait();

        //НеФП
        if (result.isPresent()) {
            tagName = result.get().trim();

            tvTasks.getItems().clear();
            tvTasks.getItems().addAll(taskDao.getUnfinishedTasksByTag(tagName));
            tvTasks.getSortOrder().clear();
            tcNameTask.setSortType(TableColumn.SortType.ASCENDING);
            tvTasks.getSortOrder().addAll(tcNameTask);
        }
        isFiltredTasksInTvTasks = true;
    }

    public void OnActionMiFourWithFarDeadlineWithoutTag(ActionEvent actionEvent) {
        tvTasks.getItems().clear();
        tvTasks.getItems().addAll(taskDao.getFourWithFarDeadlineWithoutTag());
        tvTasks.getSortOrder().clear();
        tcNameTask.setSortType(TableColumn.SortType.ASCENDING);
        tcDeadline.setSortType(TableColumn.SortType.ASCENDING);
        tvTasks.getSortOrder().addAll(tcDeadline, tcNameTask);
        isFiltredTasksInTvTasks = true;
    }

    public void OnActionMiThreeWithNearestDeadlineWithTag(ActionEvent actionEvent) {
        String tagName;
        TextInputDialog dialog = new TextInputDialog();

        dialog.setTitle("Введите тэг");
        dialog.setHeaderText("");
        dialog.setContentText("Тэг:");
        //ФП
        Optional<String> result = dialog.showAndWait();
        //НеФП
        if (result.isPresent()) {
            tagName = result.get().trim();
            tvTasks.getItems().clear();
            tvTasks.getItems().addAll(taskDao.getThreeWithNearestDeadlineWithTag(tagName));
            tvTasks.getSortOrder().clear();
            tcNameTask.setSortType(TableColumn.SortType.ASCENDING);
            tcDeadline.setSortType(TableColumn.SortType.ASCENDING);
            tvTasks.getSortOrder().addAll(tcDeadline, tcNameTask);
        } else
            return;
        isFiltredTasksInTvTasks = true;
    }

    public void OnActionMiOnNearestMonthWithTagAndSubStr(ActionEvent actionEvent) {
        String tagName;
        TextInputDialog dialog = new TextInputDialog();

        dialog.setTitle("Введите тэг");
        dialog.setHeaderText("");
        dialog.setContentText("Тэг:");
        //ФП
        Optional<String> result = dialog.showAndWait();
        //НеФП
        if (result.isPresent()) {
            tagName = result.get().trim();
            String subStr;
            dialog = new TextInputDialog();

            dialog.setTitle("Введите подстроку");
            dialog.setHeaderText("");
            dialog.setContentText("Подстрока:");
            result = dialog.showAndWait();
            //ФП

            //НеФП
            if (result.isPresent()) {
                subStr = result.get().trim();
                tvTasks.getItems().clear();
                tvTasks.getItems().addAll(taskDao.getOnNearestMonthWithTagAndSubStr(tagName, subStr));
                tvTasks.getSortOrder().clear();
                tcNameTask.setSortType(TableColumn.SortType.ASCENDING);
                tcDeadline.setSortType(TableColumn.SortType.ASCENDING);
                tvTasks.getSortOrder().addAll(tcDeadline, tcNameTask);
            } else
                return;
        } else
            return;
        isFiltredTasksInTvTasks = true;
    }

    public void OnActionMiOverdueWithOneMoreTagFromThreePopular(ActionEvent actionEvent) {
        tvTasks.getItems().clear();
        tvTasks.getItems().addAll(taskDao.getOverdueWithOneMoreTagFromThreePopular());
        tvTasks.getSortOrder().clear();
        tcNameTask.setSortType(TableColumn.SortType.ASCENDING);
        tcDeadline.setSortType(TableColumn.SortType.ASCENDING);
        tvTasks.getSortOrder().addAll(tcDeadline, tcNameTask);
        isFiltredTasksInTvTasks = true;
    }
}
